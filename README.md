# reading_python_for_data_analysis

Pythonによるデータ分析入門 第2版の読書会

1章 はじめに

2章 Pythonの基礎、IPythonとJupyter Notebook

3章 Python組み込みのデータ構造と関数、ファイルの扱い

4章 NumPyの基礎:配列とベクトル演算

5章 pandas入門

6章 データの読み込み、書き出しとファイル形式

7章 データのクリーニングと前処理

8章 データラングリング:連結、結合、変形

9章 プロットと可視化

10章 データの集約とグループ演算

11章 時系列データ

12章 pandas:応用編

13章 Pythonにおけるモデリングライブラリ入門

14章 データ分析の実例

付録A NumPy:応用編
付録B IPythonシステム上級編
